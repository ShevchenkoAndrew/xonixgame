﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class CellConfigRedactor : EditorWindow
{
    private CellConfig _cellConfig;

    private string _nameConfig;

    private Vector2 _scrollPosition = Vector2.zero;

    private float _width;

    private int _numberAddedColumn;
    private int _numberRemovableColumn;
    
    private int _numberAddedRow;
    private int _numberRemovableRow;
    
    [MenuItem("Xonix/OpenConfigRedactor")]
    private static void OpenWindow()
    {
        EditorWindow window = GetWindow(typeof(CellConfigRedactor));
            
        Rect rectWindow = window.position;
        rectWindow.center = Vector2.zero;
        rectWindow.position = new Vector2(1000, 200);

        window.position = rectWindow;
    }

    public void Init(string nameConfig)
    {
        string pathToConfig = string.Format("{0}/{1}", CellConfig.PATH_TO_FOLDER_CONFIGS, nameConfig);
        CellConfig cellConfig = Resources.Load<CellConfig>(pathToConfig);
        if (cellConfig == null)
        {
            Close();
            return;
        }

        _cellConfig = cellConfig;
        _nameConfig = nameConfig;
        this.titleContent.text = nameConfig;

        CancleSelectionCells();
        Repaint();
    }

    private void OnEnable()
    {
        _width = 130f;
        
        CancleSelectionCells();
    }
    
    void OnGUI()
    {
        ViewGeneralConfigButtons();

        if (!CheckValidConfig())
        {
            return;
        }
        
        ViewGeneralDataButtons();

        ViewButtonsForChangeGroupCells();

        GUILayout.BeginArea(new Rect(0, 85, position.width, position.height));
        
        GUILayout.Space(5);

        Handles.BeginGUI();
        Handles.DrawLine(new Vector3(0, 0), new Vector3(position.width, 0));
        Handles.EndGUI();

        _scrollPosition = GUILayout.BeginScrollView(_scrollPosition, GUILayout.Width(position.width),
            GUILayout.Height(position.height - 90));

        ViewDataConfig();
        
//        if (GUI.changed)
//        {
//            EditorUtility.SetDirty(_cellConfig);
//        }

        GUILayout.EndScrollView();
        GUILayout.EndArea();
    }

    private void ViewGeneralConfigButtons()
    {
        GUILayout.BeginArea(new Rect(5, 5, 500, 25));
        GUILayout.BeginHorizontal();
     
        if (GUILayout.Button("OpenConfig", GUILayout.ExpandWidth(false)))
        {
            OpenConfigWindow.OpenWindow(position.center, Init);
        }
        
        GUILayout.Space(20);
        
        if (GUILayout.Button("CreateConfig", GUILayout.ExpandWidth(false)))
        {
            CreateConfigWindow.OpenWindow(position.center);
        }
        
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    private bool _isSelectionCells = false;
    private List<CellConfig.CellParameters> _selectionCell = new List<CellConfig.CellParameters>();
    
    private void ViewButtonsForChangeGroupCells()
    {
        GUILayout.BeginArea(new Rect(605, 30, 500, 75));

        if (!_isSelectionCells)
        {
            if (GUILayout.Button("Begin selection group cells", GUILayout.ExpandWidth(false)))
            {
                _isSelectionCells = true;
            }
            
            GUILayout.EndArea();
            return;
        }

        if (GUILayout.Button("Cancle selection group cells", GUILayout.ExpandWidth(false)))
        {
            CancleSelectionCells();
            
            GUILayout.EndArea();
            return;
        }
        
        GUILayout.Space(5);
        
        GUILayout.BeginHorizontal();
        
        if (GUILayout.Button("Edit group cells", GUILayout.ExpandWidth(false)) )
        {
            if (_selectionCell.Count == 0)
            {
                Debug.Log("Не было выбранно ни одной ячейки");
            }
            else
            {
                CellConfig.CellParameters temp = new CellConfig.CellParameters();
            
                CellParameterWindow.OpenWindow(position.center, "GROUP", temp, () =>
                {
                    foreach (CellConfig.CellParameters cellParameters in _selectionCell)
                    {
                        if (cellParameters == null)
                        {
                            continue;
                        }
                        
                        cellParameters.CoppyValues(temp);
                    }
                    
                    CancleSelectionCells();
                
                    Repaint();
                    
                    
                    EditorUtility.SetDirty(_cellConfig);
                });
            }
        }

        GUILayout.Space(20);
        
        GUILayout.Label(string.Format("Выделенно {0} ячеек.", _selectionCell.Count), EditorStyles.boldLabel, GUILayout.ExpandWidth(false));

        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    private void CancleSelectionCells()
    {
        _selectionCell.Clear();
        _isSelectionCells = false;
    }

    private void ViewGeneralDataButtons()
    {
        GUILayout.BeginArea(new Rect(5, 30, 500, 75));
        
        if (_isSelectionCells)
        {
            EditorGUILayout.HelpBox("Вы не можете изменять кол-во столбцов и колонок в таблице сейчас.", MessageType.Warning);
            Repaint();
            
            GUILayout.EndArea();
            return;
        }
        
        GUILayout.BeginHorizontal();
        
        if (GUILayout.Button(string.Format("Add new column between {0} and {1}", _numberAddedColumn - 1, _numberAddedColumn), GUILayout.ExpandWidth(false)))
        {
            _cellConfig.AddNewColumn(_numberAddedColumn);
        }
        
        _numberAddedColumn = EditorGUILayout.IntField(_numberAddedColumn, GUILayout.Width(_width / 3), GUILayout.Height(19));
        
        GUILayout.Space(50);
        
        if (GUILayout.Button("Removable colum :", GUILayout.ExpandWidth(false)))
        {
            _cellConfig.RemoveColumn(_numberRemovableColumn);
        }
        
        _numberRemovableColumn = EditorGUILayout.IntField(_numberRemovableColumn, GUILayout.Width(_width / 3), GUILayout.Height(19));
        GUILayout.EndHorizontal();
        
        GUILayout.Space(5);
        
        GUILayout.BeginHorizontal();
        if (GUILayout.Button(string.Format("Add new row between {0} and {1}", _numberAddedRow - 1, _numberAddedRow), GUILayout.ExpandWidth(false)))
        {
            _cellConfig.AddNewRow(_numberAddedRow);
        }
        
        _numberAddedRow = EditorGUILayout.IntField(_numberAddedRow, GUILayout.Width(_width / 3), GUILayout.Height(19));
        
        GUILayout.Space(50);
        
        if (GUILayout.Button("Removable row :", GUILayout.ExpandWidth(false)))
        {
            _cellConfig.RemoveRow(_numberRemovableRow);
        }
        
        _numberRemovableRow = EditorGUILayout.IntField(_numberRemovableRow, GUILayout.Width(_width / 3), GUILayout.Height(19));
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    private bool CheckValidConfig()
    {
        GUILayout.BeginArea(new Rect(0, 30, position.width, position.height));
        
        if (_cellConfig == null || _nameConfig == null)
        {
            EditorGUILayout.HelpBox("_cellConfig == null || _nameConfig == null", MessageType.Warning);
            Repaint();
            
            GUILayout.EndArea();
            return false;
        }

        if (_cellConfig.TableCellParameters.Count == 0 || _cellConfig.TableCellParameters[0].ColumnCells.Count == 0)
        {
            EditorGUILayout.HelpBox("_cellConfig.TableCellParameters.Count == 0 || _cellConfig.TableCellParameters[0].ColumnCells.Count == 0", MessageType.Warning);
            Repaint();
            
            GUILayout.EndArea();
            return false;
        }
        
        GUILayout.EndArea();
        return true;
    }

    private void ViewDataConfig()
    {
        for (int j = _cellConfig.TableCellParameters[0].ColumnCells.Count -1; j >= 0 ; j--)
        {
            GUILayout.BeginHorizontal();
            
            for (int i = 0; i < _cellConfig.TableCellParameters.Count; i++)
            {
                Color _defaultColor = GUI.backgroundColor;
                
                if (_cellConfig.TableCellParameters[i].ColumnCells[j].State == CellState.Closed)
                {
                    GUI.backgroundColor = Color.yellow;
                }
                else if (_cellConfig.TableCellParameters[i].ColumnCells[j].State == CellState.Filled)
                {
                    GUI.backgroundColor = Color.cyan;
                }

                if (_isSelectionCells && _selectionCell.Contains(_cellConfig.TableCellParameters[i].ColumnCells[j]))
                {
                    GUI.backgroundColor = Color.magenta;
                }
                
                string nameButton = string.Format("{0},{1}", i, j);
                if (GUILayout.Button(nameButton, GUILayout.Height(_width / 3), GUILayout.Width(_width / 3)))
                {
                    if (_isSelectionCells)
                    {
                        if (_selectionCell.Contains(_cellConfig.TableCellParameters[i].ColumnCells[j]))
                        {
                            _selectionCell.Remove(_cellConfig.TableCellParameters[i].ColumnCells[j]);
                        }
                        else
                        {
                            _selectionCell.Add(_cellConfig.TableCellParameters[i].ColumnCells[j]);
                        }
                    }
                    else
                    {
                        CellParameterWindow.OpenWindow(position.center, nameButton, _cellConfig.TableCellParameters[i].ColumnCells[j], () =>
                        {
                            Repaint();
                            EditorUtility.SetDirty(_cellConfig);
                        });
                    }
                }

                GUI.backgroundColor = _defaultColor;
            }
            
            GUILayout.EndHorizontal();
        }
    }
    
    private static string GetPathToFolder()
    {
        return string.Format("Assets/Resources/{0}/", CellConfig.PATH_TO_FOLDER_CONFIGS);
    }

    public class CellParameterWindow : EditorWindow
    {
        private string _nameCell;

        private CellConfig.CellParameters _cellParameters;

        private Action _saveDataAction;

        public static void OpenWindow(Vector2 centerWindow, string nameCell, CellConfig.CellParameters cellParameters, Action saveDataAction)
        {
            EditorWindow window = GetWindow(typeof(CellParameterWindow));
            
            Rect rectWindow = window.position;
            rectWindow.size = new Vector2(250, 250);
            rectWindow.center = centerWindow;
            window.position = rectWindow;
            
            window.titleContent.text = nameCell;
            
            ((CellParameterWindow) window).Init(nameCell, cellParameters, saveDataAction);
        }

        private void Init(string nameCell, CellConfig.CellParameters cellParameters, Action saveDataAction)
        {
            _nameCell = nameCell;
            _cellParameters = cellParameters;
            _saveDataAction = saveDataAction;
        }

        void OnGUI()
        {
            GUILayout.Space(5);
            
            if (string.IsNullOrEmpty(_nameCell) || _cellParameters == null)
            {
                EditorGUILayout.HelpBox("string.IsNullOrEmpty(_nameCell) || _cellParameters == null", MessageType.Warning);
                Repaint();
            }

            GUILayout.Label(_nameCell, EditorStyles.boldLabel);

            GUILayout.Space(10);

            _cellParameters.State = (CellState) EditorGUILayout.EnumPopup("State", _cellParameters.State);
            _cellParameters.GraphicIndefiniteCell = (GameObject) EditorGUILayout.ObjectField("GraphicIndefiniteCell",
                _cellParameters.GraphicIndefiniteCell, typeof(GameObject), false);

            _cellParameters.GraphicFilledCell = (GameObject) EditorGUILayout.ObjectField("GraphicFilledCell",
                _cellParameters.GraphicFilledCell, typeof(GameObject), false);

            _cellParameters.CanUseSpawnMob = EditorGUILayout.Toggle("CanUseSpawnMob", _cellParameters.CanUseSpawnMob);
            
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Save", GUILayout.ExpandWidth(false)))
            {
                if (_saveDataAction != null)
                {
                    _saveDataAction();
                }
                
                Close();
            }
            
            GUILayout.Space(50);
            
            if (GUILayout.Button("Cancle", GUILayout.ExpandWidth(false)))
            {
                Close();
            }
            
            GUILayout.EndHorizontal();
            
            GUILayout.Space(5);
        }
    }
    
    public class CreateConfigWindow : EditorWindow
    {
        private int _columnCount = 0;
        private int _rowCoumt = 0;
        private string _nameCreatedConfig = "";
        
        private void OnEnable()
        {
            _nameCreatedConfig = "Test Config";
        }

        public static void OpenWindow(Vector2 centerWindow)
        {
            EditorWindow window = GetWindow(typeof(CreateConfigWindow));
            
            Rect rectWindow = window.position;
            rectWindow.size = new Vector2(250, 250);
            rectWindow.center = centerWindow;
            window.position = rectWindow;
            
            window.titleContent.text = "CreateConfig";
        }

        void OnGUI()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Name Config:",  GUILayout.ExpandWidth(false));
            _nameCreatedConfig = EditorGUILayout.TextField(_nameCreatedConfig, GUILayout.Width(120), GUILayout.Height(18));
            GUILayout.EndHorizontal();
        
            GUILayout.BeginHorizontal();
            GUILayout.Label("Column count:", GUILayout.ExpandWidth(false));
            _columnCount = EditorGUILayout.IntField(_columnCount,GUILayout.Width(40), GUILayout.Height(20));
            GUILayout.EndHorizontal();
            
            GUILayout.BeginHorizontal();
            GUILayout.Label("Row count:", GUILayout.ExpandWidth(false));
            _rowCoumt = EditorGUILayout.IntField(_rowCoumt,GUILayout.Width(40), GUILayout.Height(20));
            GUILayout.EndHorizontal();
            
            GUILayout.Space(20);
        
            if (GUILayout.Button("CreateConfig", GUILayout.ExpandWidth(false)))
            {
                CreateConfig();
                Close();
            }
        }
        
        private void CreateConfig()
        {
            CellConfig cellConfig = CreateInstance<CellConfig>();
            
            if (!Directory.Exists(GetPathToFolder()))
            {
                Directory.CreateDirectory(GetPathToFolder());
            }
            
            AssetDatabase.CreateAsset(cellConfig, GetPathToFolder() + _nameCreatedConfig +".asset");
            AssetDatabase.SaveAssets();

            cellConfig.GenerateCellParameters(_columnCount, _rowCoumt);
        }
    }
    
     public class OpenConfigWindow : EditorWindow
     {
         private Action<string> _completeAction;
         private Vector2 _scrollPosition = Vector2.zero;
         
        public static void OpenWindow(Vector2 centerWindow, Action<string> completeAction)
        {
            EditorWindow window = GetWindow(typeof(OpenConfigWindow));
            
            Rect rectWindow = window.position;
            rectWindow.size = new Vector2(250, 350);
            rectWindow.center = centerWindow;
            window.position = rectWindow;
            
            window.titleContent.text = "OpenConfig";
            
            ((OpenConfigWindow) window)._completeAction = completeAction;
        }

        void OnGUI()
        {
            GUILayout.Space(5);
            
            if (!Directory.Exists(GetPathToFolder()))
            {
                EditorGUILayout.HelpBox("Нет ни одного конфига для ячеек!", MessageType.Warning);
                Repaint();
                return;
            }
            
            _scrollPosition = GUILayout.BeginScrollView(_scrollPosition, GUILayout.Width(position.width),
                GUILayout.Height(position.height));

            ViewAllConfigs();

            GUILayout.Space(5);
            
            GUILayout.EndScrollView();
        }

        private void ViewAllConfigs()
        {
            DirectoryInfo info = new DirectoryInfo(GetPathToFolder());
            FileInfo[] fileInfos = info.GetFiles();
            
            if (fileInfos.Length == 0)
            {
                EditorGUILayout.HelpBox("Нет ни одного конфига для ячеек!", MessageType.Warning);
                Repaint();
                return;
            }
            
            foreach (FileInfo fileInfo in fileInfos)
            {
                string fileName = fileInfo.Name;
                if (!fileName.EndsWith(".asset"))
                {
                    continue;
                }

                fileName = fileName.Replace(".asset", "");
                
                GUILayout.Space(2);
                
                if (GUILayout.Button(fileName, GUILayout.ExpandWidth(false)))
                {
                    if (_completeAction != null)
                    {
                        _completeAction(fileName);
                    }
                    else
                    {
                        CellConfigRedactor.OpenWindow();
                    }
                    
                    Close();
                }
            }
        }
    }
}
