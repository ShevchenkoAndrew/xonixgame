﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController Instance;

    [SerializeField] private float _timeBeforeRespawnPlayer = 2f;
    
    [SerializeField] private PlayerBehavior _playerPrefab;
    
    [SerializeField] private BotBehavior _simpleEnemyPrefab;
    
    [SerializeField] private CellItem _playerSpawnCell;

    [SerializeField] private CameraController _cameraController;
    public CellItem PlayerSpawnCell
    {
        get
        {
            if (_playerSpawnCell == null)
            {
                _playerSpawnCell = CellController.GetPlayerSpawnCell();
            }
            
            return _playerSpawnCell;
        }
    }
    
    [SerializeField] private PlayerBehavior _playerBehavior;
    public PlayerBehavior PlayerBehavior
    {
        get
        {
            if (_playerBehavior == null)
            {
                _playerBehavior = Instantiate(_playerPrefab);
                
                SubscubeOnPlayerActions();
            }
            
            return _playerBehavior;
        }
    }
   
    [HideInInspector] public CellController CellController;

    private List<BotBehavior> _enemies;
    public List<BotBehavior> Enemies
    {
        get
        {
            if (_enemies == null)
            {
               _enemies = new List<BotBehavior>()
               {
                   Instantiate(_simpleEnemyPrefab),
                   Instantiate(_simpleEnemyPrefab),
                   Instantiate(_simpleEnemyPrefab),
                   Instantiate(_simpleEnemyPrefab),
               };
            }

            return _enemies;
        }
    }

    private void Awake()
    {
        Instance = this;
        
        CellController = GetComponent<CellController>();
    }

    private void Start()
    {
        _cameraController.Setup(PlayerSpawnCell.transform.position,
            CellController.GetFirstCellItem().transform.position,
            CellController.GetLastCellItem().transform.position);
        
        StartLevel();
        
        SpawnEnemies();
    }

    private void StartLevel()
    {
        CoroutineRunner.Instance.ApplyInvoke(_timeBeforeRespawnPlayer, SpawnPlayer);
    }
    
    private void SpawnPlayer()
    {
        PlayerBehavior.ApplySpawn(PlayerSpawnCell);
                        
        _cameraController.SetTargetMovement(_playerBehavior.transform);
    }
    
    private void SpawnEnemies()
    {
        List<CellItem> spawnSell = new List<CellItem>(CellController.CellItemsForSpawnWaterMobs);
        
        foreach (BotBehavior enemy in Enemies)
        {
            enemy.name = enemy.name + Enemies.IndexOf(enemy);
            
            if (spawnSell.Count == 0)
            {
                Debug.Log("Нет доступных точек спавна для врага.");
                return;
            }

            int randomValue = Random.Range(0, spawnSell.Count);
            
            SubscubeOnBotActions(enemy);
            enemy.ApplySpawn(spawnSell[randomValue]);
            
            spawnSell.RemoveAt(randomValue);
        }
    }

    private void ApplyPlayerDamage()
    {
        if (PlayerBehavior.CountLife <= 0)
        {
            // TODO: realization game over
            return;
        }

        StartLevel();
    }

    private void SubscubeOnPlayerActions()
    {
        PlayerBehavior.OnDestroyMortal += UnSubscubeOnMortalActions;
        PlayerBehavior.OnGetDamage += ApplyPlayerDamage;
        
        CellController.SubscubeOnPlayerActions(PlayerBehavior);
    }
    
    private void SubscubeOnBotActions(BotBehavior enemy)
    {
        enemy.OnDestroyMortal += UnSubscubeOnMortalActions;
        
        CellController.SubscubeOnBotsActions(enemy);
    }
    
    private void UnSubscubeOnMortalActions(Mortal mortal)
    {
        mortal.OnDestroyMortal -= UnSubscubeOnMortalActions;
        
        if (mortal is PlayerBehavior)
        {
            PlayerBehavior.OnGetDamage -= ApplyPlayerDamage;
            CellController.UnSubscubeOnPlayerActions(PlayerBehavior);
        }
        else
        {
            CellController.UnSubscubeOnBotsActions(mortal as BotBehavior);
        }
    }
}
