﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CellController : MonoBehaviour
{
    public static event Action<CellItem> OnAddCellInTrack;
    public static event Action<CellItem> OnDestroyPlayerCell;
    
    [SerializeField] private CellItem _cellPrefab;
    [SerializeField] private Transform _parentCellsTransform;
    [SerializeField] private GameObject _planeGo;

    [ShowOnly] [SerializeField] private int _countColumn = 0;
    [ShowOnly] [SerializeField] private int _countRow = 0;

    [ShowOnly] public float _widthCell = 0f;
    [ShowOnly] public float _depthCell = 0f;
    [ShowOnly] public float _heightCell = 0f;
    
    [SerializeField] private CellItem[,] _cellItems;
    public CellItem[,] CellItems
    {
        get { return _cellItems; }
    }

    private List<CellItem> _cellItemsForSpawnEarthMobs = new List<CellItem>();
    public List<CellItem> CellItemsForSpawnEarthMobs
    {
        get { return _cellItemsForSpawnEarthMobs; }
    }

    private List<CellItem> _cellItemsForSpawnWaterMobs = new List<CellItem>();
    public List<CellItem> CellItemsForSpawnWaterMobs
    {
        get { return _cellItemsForSpawnWaterMobs; }
    }

    private void Awake()
    {
        SetupGameField();
        SetupPlane();
    }

    private void SetupPlane()
    {
        float scaleX = _countColumn / 10f;
        float scaleZ = _countRow / 10f;
        
        _planeGo.transform.localScale = new Vector3(scaleX, 1, scaleZ);

        float posX = _countColumn / 2f;
        float posZ = _countRow / 2f;
        
        _planeGo.transform.localPosition = new Vector3(posX, 0, posZ);
    }

    [ContextMenu("GenerateGameField")]
    private void SetupGameField()
    {
        ClearAllCells();
        
        CellConfig cellConfig = CellConfig.GetConfig();

        if (!CheckValidConfig(cellConfig))
        {
            return;
        }

        _countColumn = cellConfig.TableCellParameters.Count;
        _countRow = cellConfig.TableCellParameters[0].ColumnCells.Count;
        
        _cellItems = new CellItem[_countColumn, _countRow];
        
        _widthCell = _cellPrefab.transform.localScale.x;
        _depthCell = _cellPrefab.transform.localScale.z;
        _heightCell = _cellPrefab.transform.localScale.y;

        for (int i = 0; i < _countColumn; i++)
        {
            for (int j = 0; j < _countRow; j++)
            {
                CellConfig.CellParameters cellParameters = cellConfig.TableCellParameters[i].ColumnCells[j];
                
                Vector3 tempPosition = _parentCellsTransform.position +
                                       new Vector3(i * _widthCell + _widthCell / 2, _heightCell / 2, j * _depthCell + _depthCell / 2);

                CellItem tempCellItem = Instantiate(_cellPrefab, tempPosition, Quaternion.identity, _parentCellsTransform);
                tempCellItem.name = string.Format("Cell {0},{1}", i, j);
                tempCellItem.Init(i, j, cellParameters);

                _cellItems[i, j] = tempCellItem;
                
                

                if (cellParameters.CanUseSpawnMob)
                {
                    if (tempCellItem.IsFilledCell())
                    {
                        _cellItemsForSpawnEarthMobs.Add(tempCellItem);
                    }
                    else
                    {
                        _cellItemsForSpawnWaterMobs.Add(tempCellItem);
                    }
                }
            }
        }
    }

    private bool CheckValidConfig(CellConfig cellConfig)
    {
        if (cellConfig == null
            || cellConfig.TableCellParameters.Count == 0
            || cellConfig.TableCellParameters[0].ColumnCells.Count == 0)
        {
            Debug.Log("Конфиг не валидный");
            return false;
        }

        return true;
    }

    private void ClearAllCells()
    {
        List<CellItem> tempCellItems = _parentCellsTransform.GetComponentsInChildren<CellItem>().ToList();
        
        foreach (CellItem cellItem in tempCellItems)
        {
            DestroyImmediate(cellItem.gameObject);
        }
    }

    public List<CellItem> GetEnemyCell()
    {
        List<CellItem> cellItems = new List<CellItem>();
        
        List<BotBehavior> enemyis = GameController.Instance.Enemies;

        foreach (Mortal mortal in enemyis)
        {
            cellItems.Add(GetNearestCellItem(mortal.transform.position));
        }

        return cellItems;
    }

    private CellItem GetNearestCellItem(Vector3 point)
    {
        CellItem cellItem = null;
        float distance = 10000;
        
        for (int i = 0; i < _countColumn; i++)
        {
            for (int j = 0; j < _countRow; j++)
            {
                float tempDistance = Vector3.Magnitude(_cellItems[i, j].transform.position - point);
                    
                if (tempDistance < distance)
                {
                    distance = tempDistance;
                    cellItem = _cellItems[i, j];
                }
            }
        }

        return cellItem;
    }

    public void SubscubeOnPlayerActions(PlayerBehavior playerBehavior)
    {
        playerBehavior.OnChangeCurrentCell += ChangeCurrentCellPlayer;
        playerBehavior.OnGetDamage += ApplyForceDestroyPlayerTrack;
    }
    
    public void UnSubscubeOnPlayerActions(PlayerBehavior playerBehavior)
    {
        playerBehavior.OnChangeCurrentCell -= ChangeCurrentCellPlayer;
        playerBehavior.OnGetDamage -= ApplyForceDestroyPlayerTrack;
    }
    
    public void SubscubeOnBotsActions(BotBehavior botBehavior)
    {
        botBehavior.OnCollisionCellItem += ApplyEnemyCollisionCell;
    }
    
    public void UnSubscubeOnBotsActions(BotBehavior botBehavior)
    {
        botBehavior.OnCollisionCellItem -= ApplyEnemyCollisionCell;
    }

    private void ApplyEnemyCollisionCell(CellItem cellItem)
    {
        if (cellItem.IsFilledCell())
        {
            return;
        }

        DestroyPlayerTrack(cellItem);
    }

    [SerializeField] private float _speedDestroyTrack = 0.5f;
    
    private void ApplyForceDestroyPlayerTrack()
    {
        foreach (CellItem cellItem in _playerTrackCellItems)
        {
            if (cellItem.IsFilledCell())
            {
                continue;
            }
            
            cellItem.ChangeCellState(CellState.Empty);

            if (OnDestroyPlayerCell != null)
            {
                OnDestroyPlayerCell(cellItem);
            }
        }
    }
    
    private void DestroyPlayerTrack(CellItem cellItem)
    {
        cellItem.ChangeCellState(CellState.Empty);
        
        if (OnDestroyPlayerCell != null)
        {
            OnDestroyPlayerCell(cellItem);
        }
        
        int index = _playerTrackCellItems.IndexOf(cellItem);

        StartCoroutine(ApplyDestroyTrack(index, true));
        StartCoroutine(ApplyDestroyTrack(index, false));
    }

    private IEnumerator ApplyDestroyTrack(int indexCell, bool leftTrack)
    {
        yield return new WaitForSeconds(_speedDestroyTrack);
       
        while (true)
        {
            indexCell = leftTrack ? --indexCell : ++indexCell;
            
            if (indexCell < 0 
                || indexCell > _playerTrackCellItems.Count -1 
                ||  _playerTrackCellItems[indexCell].IsFilledCell() 
                || _playerTrackCellItems[indexCell].CurrentCellState == CellState.Empty)
            {
                yield break;
            }

            _playerTrackCellItems[indexCell].ChangeCellState(CellState.Empty);

            if (OnDestroyPlayerCell != null)
            {
                OnDestroyPlayerCell(_playerTrackCellItems[indexCell]);
            }
            
            yield return new WaitForSeconds(_speedDestroyTrack);
        }
        
        yield return null;
    }

    private List<CellItem> _playerTrackCellItems = new List<CellItem>();
    private void ChangeCurrentCellPlayer(CellItem cellItem)
    {
        if (cellItem == null)
        {
            return;
        }
        
        int count = _playerTrackCellItems.Count;

        if (count > 0)
        {
            if (_playerTrackCellItems[count - 1].IsFilledCell())
            {
                
            }
        }
        
        if (OnAddCellInTrack != null)
        {
            OnAddCellInTrack(cellItem);
        }

        
        
        if (cellItem.IsFilledCell() && count > 0 && !_playerTrackCellItems[count - 1].IsFilledCell())
        {
            ApplyFillCells();
            _playerTrackCellItems = new List<CellItem>(){cellItem};
        }

        _playerTrackCellItems.Add(cellItem);
    }

    private void ApplyFillCells()
    {
        HashSet<CellItem> enemyCells = new HashSet<CellItem>(){};

        foreach (CellItem cellItem in GetEnemyCell())
        {
            GetEnemyCellItems(cellItem, ref enemyCells);
        }

        for (int i = 0; i < _countColumn; i++)
        {
            for (int j = 0; j < _countRow; j++)
            {
                if (enemyCells.Contains(_cellItems[i, j]))
                {
                    continue;
                }
                
                _cellItems[i,j].ChangeCellState(CellState.Filled);
            }
        }
    }

    private void GetEnemyCellItems(CellItem cellItem, ref HashSet<CellItem> enemySells)
    {
        if (cellItem.CurrentCellState != CellState.Empty)
        {
            return;
        }
        
        CellItem leftCell = GetCellItem(cellItem.Column - 1, cellItem.Row);
        CellItem rightCell = GetCellItem(cellItem.Column + 1, cellItem.Row);
        CellItem topCell = GetCellItem(cellItem.Column, cellItem.Row + 1);
        CellItem downCell = GetCellItem(cellItem.Column, cellItem.Row - 1);

        if (leftCell != null && Equals(leftCell.CurrentCellState, CellState.Empty)
            || rightCell != null && Equals(rightCell.CurrentCellState, CellState.Empty)
            || topCell != null && Equals(topCell.CurrentCellState, CellState.Empty)
            || downCell != null && Equals(downCell.CurrentCellState, CellState.Empty))
        {
            enemySells.Add(cellItem);
        }
        else
        {
            return;
        }
        
        for (int i = -1; i < 2; i++)
        {
            for (int j = -1; j < 2; j++)
            {
                CellItem tempCellItem = GetCellItem(cellItem.Column + i, cellItem.Row + j);

                if (tempCellItem == null || enemySells.Contains(tempCellItem))
                {
                    continue;
                }

                GetEnemyCellItems(tempCellItem, ref enemySells);
            }
        }
    }

    public CellItem GetPlayerSpawnCell()
    {
        bool evenColumnCount = _countColumn % 2 == 0;
        int addColumnCount = evenColumnCount ? -1 : 0;

        return _cellItems[_countColumn / 2 + addColumnCount, 0];
    }

    public CellItem GetFirstCellItem()
    {
        return GetCellItem(0, 0);
    }

    public CellItem GetLastCellItem()
    {
        return GetCellItem(_countColumn - 1, _countRow - 1);
    }
    
    public CellItem GetCellItem(CellItem currentCellItem, MovementComponent.DirectionMovement direction)
    {
        int newColumn = -1;
        int newRow = -1;
        
        
        switch (direction)
        {
            case MovementComponent.DirectionMovement.Forvard:
                newColumn = currentCellItem.Column;
                newRow = currentCellItem.Row + 1;
                break;
            case MovementComponent.DirectionMovement.Back:
                newColumn = currentCellItem.Column;
                newRow = currentCellItem.Row - 1;
                break;
            case MovementComponent.DirectionMovement.Left:
                newColumn = currentCellItem.Column - 1;
                newRow = currentCellItem.Row;
                break;
            case MovementComponent.DirectionMovement.Right:
                newColumn = currentCellItem.Column + 1;
                newRow = currentCellItem.Row;
                break;
            default:
                Debug.LogFormat("Не знаю такого направления: {0}!", direction);
                break;
        }

        return GetCellItem(newColumn, newRow);
    }
    
    private CellItem GetCellItem(int column, int row)
    {
        if (!CheckValidCell(column, row))
        {
            return null;
        }
        
        return _cellItems[column, row];
    }

    private bool CheckValidCell(int column, int row)
    {
        return CheckValidColumn(column) && CheckValidRow(row);
    }

    private bool CheckValidColumn(int column)
    {
        return column <= _countColumn - 1 && column >= 0;
    }
    
    private bool CheckValidRow(int row)
    {
        return row <= _countRow - 1 && row >= 0;
    }
}