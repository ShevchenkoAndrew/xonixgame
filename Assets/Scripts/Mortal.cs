﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mortal : MonoBehaviour
{
    public event Action<Mortal> OnDestroyMortal; 
    
    protected Collider _collider;
    
    protected MovementComponent _movementComponent;

    protected virtual void Awake()
    {
        _collider = GetComponent<Collider>();
        _movementComponent = GetComponent<MovementComponent>();
    }
    
    protected virtual void Start()
    {
    }
    
    protected virtual void OnDestroy()
    {
        if (OnDestroyMortal != null)
        {
            OnDestroyMortal(this);
        }
    }
    
    public virtual void ApplySpawn(CellItem cellItem)
    {
        transform.position = GetDesiredPositon(cellItem);
        transform.rotation = cellItem.transform.rotation;
    }

    protected virtual bool StartMovement()
    {
        if (!_movementComponent.CanMove)
        {
            return false;
        }

        return true;
    }

    protected virtual Vector3 GetDesiredPositon(CellItem cellItem)
    {
        return cellItem.transform.position + new Vector3(0, cellItem.transform.localScale.y + transform.localScale.y / 2, 0);
    }
    
    public void StopMovement()
    {
        _movementComponent.StopMovement();
    }
}
