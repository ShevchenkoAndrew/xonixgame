﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CellConfig : ScriptableObject
{
    public const string PATH_TO_FOLDER_CONFIGS = "CellConfigs";

    [System.Serializable]
    public class ColumnsCells
    {
        public List<CellParameters> ColumnCells = new List<CellParameters>();
    }
    
    [System.Serializable]
    public class CellParameters
    {
        public CellState State;
        public GameObject GraphicFilledCell;
        public GameObject GraphicIndefiniteCell;

        public bool CanUseSpawnMob;

        public void CoppyValues(CellParameters cellParameters)
        {
            State = cellParameters.State;
            GraphicFilledCell = cellParameters.GraphicFilledCell;
            GraphicIndefiniteCell = cellParameters.GraphicIndefiniteCell;
            CanUseSpawnMob = cellParameters.CanUseSpawnMob;
        }
    }

    [HideInInspector] public List<ColumnsCells> TableCellParameters;

    // TODO: DELETE ME
    public static CellConfig GetConfig() 
    {
        string pathToConfig = string.Format("{0}/{1}", CellConfig.PATH_TO_FOLDER_CONFIGS, "FirstConfig");
        return Resources.Load<CellConfig>(pathToConfig);
    }

    public void GenerateCellParameters(int countColumn, int countRow)
    {
        TableCellParameters = new List<ColumnsCells>();
        
        for (int i = 0; i < countColumn; i++)
        {
            List<CellParameters> columnCells = new List<CellParameters>();
            
            for (int j = 0; j < countRow; j++)
            {
                columnCells.Add(GetCellParameters(i, j, countColumn, countRow));
            }
            
            TableCellParameters.Add(new ColumnsCells(){ColumnCells = columnCells});
        }
    }

    private CellParameters GetCellParameters(int i, int j, int countColumn, int countRow)
    {
        CellParameters tempCellParameters = new CellParameters();
        bool isClosed = i == 0 || j == 0 || i == countColumn - 1 || j == countRow - 1;
        tempCellParameters.State = isClosed ? CellState.Closed : CellState.Empty;
        return tempCellParameters;
    }

    public void AddNewColumn(int numberAddedColumn)
    {
        if (numberAddedColumn <= 0 || numberAddedColumn >= TableCellParameters.Count)
        {
            Debug.Log("Не могу добавить новую колонку. numberAddedColumn <= 0 || numberAddedColumn >= TableCellParameters.Count - 1");
            return;
        }
        
        int newCountColumn = TableCellParameters.Count + 1;
        int countRow = TableCellParameters[0].ColumnCells.Count;
        
        List<CellParameters> columnCells = new List<CellParameters>();

        for (int j = 0; j < countRow; j++)
        {
            columnCells.Add(GetCellParameters(numberAddedColumn, j, newCountColumn, countRow));
        }
        
        TableCellParameters.Insert(numberAddedColumn, new ColumnsCells(){ColumnCells = columnCells});
    }

    public void RemoveColumn(int numberRemovableColumn)
    {
        if (numberRemovableColumn <= 0 || numberRemovableColumn >= TableCellParameters.Count - 1)
        {
            Debug.Log("Не могу удалить выбранную колонку. numberRemovableColumn <= 0 || numberRemovableColumn >= TableCellParameters.Count - 1");
            return;
        }
        
        TableCellParameters.RemoveAt(numberRemovableColumn);
    }

    public void AddNewRow(int numberAddedRow)
    {
        if (numberAddedRow <= 0 || numberAddedRow >= TableCellParameters[0].ColumnCells.Count)
        {
            Debug.Log("Не могу добавить новую строку. numberAddedRow <= 0 || numberAddedRow >= TableCellParameters[0].ColumnCells.Count - 1");
            return;
        }
        
        int countColumn = TableCellParameters.Count;
        int newCountRow = TableCellParameters[0].ColumnCells.Count + 1;

        for (int i = 0; i < TableCellParameters.Count; i++)
        {
            TableCellParameters[i].ColumnCells.Insert(numberAddedRow, GetCellParameters(i, numberAddedRow, countColumn, newCountRow));
        }
    }
    
    public void RemoveRow(int numberRemovableRow)
    {
        if (numberRemovableRow <= 0 || numberRemovableRow >= TableCellParameters[0].ColumnCells.Count - 1)
        {
            Debug.Log("Не могу удалить выбранную строку. numberRemovableRow <= 0 || numberRemovableRow >= TableCellParameters[0].ColumnCells.Count - 1");
            return;
        }

        foreach (ColumnsCells columnCells in TableCellParameters)
        {
            columnCells.ColumnCells.RemoveAt(numberRemovableRow);
        }
    }
}
