﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CellState
{
    Empty,
    Temporary,
    Filled,
    Closed,
}

public class CellItem : MonoBehaviour
{
    public event Action<CellState> OnChangeState;
    
    [SerializeField] private GameObject _graphicFilledCell;
    [SerializeField] private GameObject _graphicIndefiniteCell;
    
    [SerializeField] private BoxCollider _boxCollider;

    private CellState _currentCellState = CellState.Empty;
    public CellState CurrentCellState
    {
        get { return _currentCellState; }
    }
    
    private int _column;
    public int Column
    {
        get { return _column; }
        set { _column = value; }
    }
    
    private int _row;
    public int Row
    {
        get { return _row; }
        set { _row = value; }
    }

    private void Awake()
    {
        if (_graphicFilledCell == null)
        {
            Debug.Log("Ссылка на _graphicFilledCell отсутствует!");
        }
        
        if (_graphicIndefiniteCell == null)
        {
            Debug.Log("Ссылка на _graphicIndefiniteCell отсутствует!");
        }
        
        if (_boxCollider == null)
        {
            Debug.Log("Ссылка на компонент BoxCollider отсутствует!");
        }
    }

    public void Init(int col, int row, CellConfig.CellParameters cellParameters)
    {
        _column = col;
        _row = row;

        _currentCellState = cellParameters.State;

        if (cellParameters.GraphicIndefiniteCell != null)
        {
            if (_graphicIndefiniteCell != null)
            {
                DestroyImmediate(_graphicIndefiniteCell);
            }

            _graphicIndefiniteCell = Instantiate(cellParameters.GraphicIndefiniteCell, transform);
            _graphicIndefiniteCell.transform.localPosition = Vector3.zero;
            _graphicIndefiniteCell.transform.rotation = Quaternion.identity;
            _graphicIndefiniteCell.name = "GraphicFilledCell";
        }

        if (cellParameters.GraphicFilledCell != null)
        {
            if (_graphicFilledCell != null)
            {
                DestroyImmediate(_graphicFilledCell);
            }

            _graphicFilledCell = Instantiate(cellParameters.GraphicFilledCell, transform);
            _graphicFilledCell.transform.localPosition = Vector3.zero;
            _graphicFilledCell.transform.rotation = Quaternion.identity;
            _graphicFilledCell.name = "GraphicFilledCell";
        }
        
        ChangeCellState();
    }

    public void ChangeCellState(CellState cellState)
    {
        if (!CanSetCellState(cellState))
        {
            return;
        }

        _currentCellState = cellState;
        
        ChangeCellState();
        
        if (OnChangeState != null)
        {
            OnChangeState(_currentCellState);
        }
    }

    private bool CanSetCellState(CellState cellState)
    {
        return !Equals(_currentCellState, CellState.Closed)
               && (!Equals(_currentCellState, CellState.Filled) || !Equals(cellState, CellState.Temporary))
               && !Equals(_currentCellState, cellState);
    }

    private void ChangeCellState()
    {
        switch (_currentCellState)
        {
            case CellState.Empty:
                _graphicIndefiniteCell.SetActive(false);
                _graphicFilledCell.SetActive(false);
                _boxCollider.enabled = false;
                break;
            case CellState.Temporary:
                _graphicIndefiniteCell.SetActive(true);
                _graphicFilledCell.SetActive(false);
                _boxCollider.enabled = true;
                break;
            default:
                _graphicIndefiniteCell.SetActive(false);
                _graphicFilledCell.SetActive(true);
                _boxCollider.enabled = true;
                break;
        }
    }
    
    public bool IsFilledCell()
    {
        return _currentCellState == CellState.Closed || _currentCellState == CellState.Filled;
    }
}
