﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Vector3 _startOffset;
    [SerializeField] private Vector3 _edgeOffset;
    
    [SerializeField] private Vector3 _currentOffset;

    [SerializeField] private float _speedMovement = 3f;
    
    private Transform _targetLook;
    private Transform _targetMovement;

    private Quaternion _defaultRotation;

    private Vector3 _startPositon;

    private Vector3 _leftBottomEdgePosition;

    private Vector3 _rightTopEdgePosition;

    private void Awake()
    {
        _defaultRotation = transform.rotation;
    }

    public void Setup(Vector3 positionStart, Vector3 positonFirstCell, Vector3 positonLastCell)
    {
        transform.position = positionStart + _startOffset;
        SetupEdges(positonFirstCell, positonLastCell);    
        
#if UNITY_EDITOR
        _positonFirstCell = positonFirstCell;
        _positonLastCell = positonLastCell;
#endif
    }

#if UNITY_EDITOR
    private Vector3 _positonFirstCell;
    private Vector3 _positonLastCell;
    
    [ContextMenu("SetupEdges")]
    private void SetupEdges()
    {
        SetupEdges(_positonFirstCell, _positonLastCell);
    }
#endif

    private void SetupEdges(Vector3 positonFirstCell, Vector3 positonLastCell)
    {
        _leftBottomEdgePosition = new Vector3(
            positonFirstCell.x + _edgeOffset.x + _currentOffset.x, 
            0, 
            positonFirstCell.z + _edgeOffset.z + _currentOffset.z);
        _rightTopEdgePosition = new Vector3(
            positonLastCell.x - _edgeOffset.x + _currentOffset.x, 
            0, 
            positonLastCell.z - _edgeOffset.z + _currentOffset.z);
    }

    public void SetTargetMovement(Transform target)
    {
        _targetMovement = target;
    }
    
    public void SetTargetLook(Transform target)
    {
        _targetLook = target;
    }

    private void LateUpdate()
    {
        ApplyLook();
        ApplyMovement();
    }
    
    private void ApplyLook()
    {
        if (_targetLook == null)
        {
            return;
        }
        
        transform.LookAt(_targetLook);
    }

    private void ApplyMovement()
    {
        if (_targetMovement == null)
        {
            return;
        }

        Vector3 desiredPositon = _currentOffset + _targetMovement.position;

        float x = desiredPositon.x;
        float z = desiredPositon.z;

        x = Mathf.Clamp(x, _leftBottomEdgePosition.x, _rightTopEdgePosition.x);
        z = Mathf.Clamp(z, _leftBottomEdgePosition.z, _rightTopEdgePosition.z);
        
        desiredPositon = new Vector3(x, desiredPositon.y, z);
        
        transform.position =  Vector3.MoveTowards(transform.position, desiredPositon, _speedMovement * Time.deltaTime);
    }
    

}
