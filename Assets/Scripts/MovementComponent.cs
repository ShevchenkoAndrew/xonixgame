﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementComponent : MonoBehaviour
{
    public enum DirectionMovement
    {
        None,
        Forvard,
        Back,
        Left,
        Right,
        ForvardRight,
        ForvardLeft,
        BackRight,
        BackLeft,
    }
    
    [SerializeField] private float _speedMovement = 3;
    
    public bool CanMove
    {
        get { return _moveToTargetCoroutine == null && _moveToDirectionCoroutine == null && _currentTargetPosition == DEFAULT_VECTOR_3;  }
    }

    private Action _currentCompleteAction;
    private static readonly Vector3 DEFAULT_VECTOR_3 = new Vector3(-999, -999, -999);
    private Vector3 _currentTargetPosition = DEFAULT_VECTOR_3;
    
    public void StartMovementToTarget(Vector3 targetPosition, Action completeAction = null)
    {
        if (_currentTargetPosition != DEFAULT_VECTOR_3)
        {
            Debug.Log("Я все еще иду.Я скажу, когда закончу!"); 
            return;
        }

        _currentCompleteAction = completeAction;
        _currentTargetPosition = targetPosition;
        
//        if (_moveToTargetCoroutine != null)
//        {
//           Debug.Log("Я все еще иду.Я скажу, когда закончу!"); 
//           return;
//        }
//        _moveToTargetCoroutine = StartCoroutine(MoveToTarget(targetPosition, completeAction));
    }
    
    public void StartMovementOnDirection(DirectionMovement directionMovement)
    {
        if (_moveToDirectionCoroutine != null)
        {
            Debug.Log("Я все еще иду.Я скажу, когда закончу!"); 
            return;
        }

        _moveToDirectionCoroutine = StartCoroutine(MoveOnDirection(GetDirection(directionMovement)));
    }
    private Dictionary<DirectionMovement, Vector3> _directionConvertor = new Dictionary<DirectionMovement, Vector3>()
    {
        {DirectionMovement.Forvard, Vector3.forward},
        {DirectionMovement.Back, Vector3.back},
        {DirectionMovement.Right, Vector3.right},
        {DirectionMovement.Left, Vector3.left},
        {DirectionMovement.ForvardRight, new Vector3(1, 0, 1)},
        {DirectionMovement.ForvardLeft, new Vector3(-1, 0, 1)},
        {DirectionMovement.BackRight, new Vector3(1, 0, -1)},
        {DirectionMovement.BackLeft, new Vector3(-1, 0, -1)},
    };

    public Vector3 GetDirection(DirectionMovement directionMovement)
    {
        if (!_directionConvertor.ContainsKey(directionMovement))
        {
            Debug.LogFormat("Не знаю такого направления: {0}", directionMovement);
            return Vector3.zero;
        }

        return _directionConvertor[directionMovement];
    }

    private void Update()
    {
        if (_currentTargetPosition == DEFAULT_VECTOR_3)
        {
            return;
        }

        if (transform.position == _currentTargetPosition)
        {
            _currentTargetPosition = DEFAULT_VECTOR_3;

            Action tempAction = _currentCompleteAction;
            
            _currentCompleteAction = null;
            
            if (tempAction != null)
            {
                tempAction();
            }
            
            return;
        }
        
        transform.position = Vector3.MoveTowards(transform.position, _currentTargetPosition, _speedMovement * Time.deltaTime);
    }

    public void StopMovement()
    {
        if (_moveToTargetCoroutine != null)
        {
            StopCoroutine(_moveToTargetCoroutine);
            _moveToTargetCoroutine = null;
        }

        if (_moveToDirectionCoroutine != null)
        {
            StopCoroutine(_moveToDirectionCoroutine);
            _moveToDirectionCoroutine = null;
        }

        _currentTargetPosition = DEFAULT_VECTOR_3;
        _currentCompleteAction = null;
    }

    private Coroutine _moveToTargetCoroutine;
    private IEnumerator MoveToTarget(Vector3 targetPosition, Action completeAction)
    {
        while (transform.position != targetPosition)
        {

            transform.position = Vector3.MoveTowards(transform.position, targetPosition, _speedMovement * Time.deltaTime);
            yield return null;
        }
        
        _moveToTargetCoroutine = null;

        if (completeAction != null)
        {
            completeAction();
        }
    }
    
    private Coroutine _moveToDirectionCoroutine;
    private IEnumerator MoveOnDirection(Vector3 direction)
    {
        while (true)
        {
            transform.position += direction.normalized * _speedMovement * Time.deltaTime;
            
            yield return null;
        }
        
        yield return null;
    }
}
