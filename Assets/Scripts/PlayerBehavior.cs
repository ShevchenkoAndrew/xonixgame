﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehavior : Mortal
{
    [SerializeField] private int _startCountLife = 3;
    public event Action<CellItem> OnChangeCurrentCell; 
    public event Action OnGetDamage; 
    public event Action<int> OnChangeCountLife; 
    
    private CellItem _currentCellItem;
    public CellItem CurrentCellItem
    {
        get { return _currentCellItem; }
        private set
        {
            if (_currentCellItem != null)
            {
                _currentCellItem.OnChangeState -= ChangeStateCurrentCell;
            }
            
            if (value != null && value.CurrentCellState == CellState.Temporary)
            {
                GetDamage();
                _currentCellItem = null;
                return;
            }
            
            _currentCellItem = value;
            
            _currentCellItem.ChangeCellState(CellState.Temporary);
            
            _currentCellItem.OnChangeState += ChangeStateCurrentCell;
            
            if (OnChangeCurrentCell != null)
            {
                OnChangeCurrentCell(_currentCellItem);
            }
        }
    }

    private int _countLife;

    public int CountLife
    {
        get { return _countLife; }
        private set
        {
            _countLife = value;

            if (OnChangeCountLife != null)
            {
                OnChangeCountLife(_countLife);
            }
        }
    }
    
    private CellItem _desiredCellItem;

    protected MovementComponent.DirectionMovement _desiredDirectionMovement;

    private bool _canMove = false;

    protected override void Awake()
    {
        base.Awake();

        CountLife = _startCountLife;
    }
    
    protected override void OnDestroy()
    {
        base.OnDestroy();
        
        if (_currentCellItem != null)
        {
            _currentCellItem.OnChangeState -= ChangeStateCurrentCell;
        }
    }

    public override void ApplySpawn(CellItem cellItem)
    {
        base.ApplySpawn(cellItem);

        CurrentCellItem = cellItem;

        _canMove = true;
    }
        
    public void StartMovement(MovementComponent.DirectionMovement direction)
    {
        _desiredDirectionMovement = direction;
        
        StartMovement();
    }

    protected override bool StartMovement()
    {
        if (!base.StartMovement() || !_canMove)
        {
            return false;
        }

        _desiredCellItem = GameController.Instance.CellController.GetCellItem(_currentCellItem, _desiredDirectionMovement);

        if (_desiredCellItem == null)
        {
            return false;
        }
        
        _movementComponent.StartMovementToTarget(GetDesiredPositon(_desiredCellItem), CompleteMovement);

        return true;
    }

    private void CompleteMovement()
    {
        CurrentCellItem = _desiredCellItem;
        
        StartMovement();
    }

    private void ChangeStateCurrentCell(CellState cellState)
    {
        if (cellState != CellState.Empty)
        {
            return;   
        }
        
        GetDamage();
    }

    private void GetDamage()
    {
        StopMovement();
        _canMove = false;

        CountLife--;

        if (OnGetDamage != null)
        {
            OnGetDamage();
        }
    }

    [ContextMenu("Test")]
    private void Test()
    {
        _canMove = false;
    }
//    
//    private void OnCollisionEnter(Collision other)
//    {
//        Mortal mortal = other.gameObject.GetComponent<Mortal>();
//
//        if (mortal != null)
//        {
//            Debug.LogFormat("Collision. {0} {1} {2}", name, mortal.name, Time.frameCount);
//            
////            ApplyCollisionMortal(mortal, other.contacts[0]);
//            return;
//        }
//
//        CellItem cellItem = other.gameObject.GetComponent<CellItem>();
//
//        if (cellItem != null)
//        {
//            Debug.LogFormat("Collision. {0} {1} {2}", name, cellItem.name, Time.frameCount);
//            
////            ApplyCollisionCell(cellItem, other.contacts[0]);
//            return;
//        }
//        
//        Debug.LogFormat("Я не знаю с кем столкнулся. {0} {1}",name, other.gameObject.name);
//    }
}
