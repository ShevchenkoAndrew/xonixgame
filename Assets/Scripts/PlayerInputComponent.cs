﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PlayerInputComponent : MonoBehaviour 
{
	private PlayerBehavior _playerBehavior;

	private void Awake()
	{
		_playerBehavior = GetComponent<PlayerBehavior>();
	}

	private void Update()
	{
		ApplyMovementInput();
	}

	private void ApplyMovementInput()
	{
		if (Input.GetKeyDown(KeyCode.W))
		{
			_playerBehavior.StartMovement(MovementComponent.DirectionMovement.Forvard);
		}

		if (Input.GetKeyDown(KeyCode.S))
		{
			_playerBehavior.StartMovement(MovementComponent.DirectionMovement.Back);
		}
        
		if (Input.GetKeyDown(KeyCode.A))
		{
			_playerBehavior.StartMovement(MovementComponent.DirectionMovement.Left);
		}
        
		if (Input.GetKeyDown(KeyCode.D))
		{
			_playerBehavior.StartMovement(MovementComponent.DirectionMovement.Right);
		}

		if (Input.GetKeyDown(KeyCode.Q))
		{
			_playerBehavior.StopMovement();
		}
	}
}
