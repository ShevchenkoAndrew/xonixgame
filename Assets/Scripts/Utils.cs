﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils 
{
    public static float SignedAngle(Vector3 a, Vector3 b, Vector3 normal)
    {
        Vector3 perpVector = Vector3.Cross(normal, a);
        float angle = Vector3.Angle(a, b);
        float out_angle = angle * Mathf.Sign(Vector3.Dot(perpVector, b));
        return out_angle;
    }

    private static uint _uniqueId = 0;
    public static uint GetUniqueId()
    {
        uint result = _uniqueId;
        _uniqueId++;
        return result;
    }
    
}
