﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineRunner : MonoBehaviour
{
    public static CoroutineRunner Instance;

    private Dictionary<uint, Coroutine> _dictionary = new Dictionary<uint, Coroutine>();

    [ContextMenu("Awake MF")]
    private void Awake()
    {
        Instance = this;
    }

    public uint ApplyInvoke(Action invokeDelegate)
    {
        uint id = Utils.GetUniqueId();

        _dictionary.Add(id, StartCoroutine(InvokeCoroutine(id, -1, invokeDelegate)));

        return id;
    }

    public uint ApplyInvoke(float time, Action invokeDelegate)
    {
        uint id = Utils.GetUniqueId();

        _dictionary.Add(id, StartCoroutine(InvokeCoroutine(id, time, invokeDelegate)));

        return id;
    }

    private IEnumerator InvokeCoroutine(uint id, float time, Action invokeDelegate)
    {
        if (time <= 0)
        {
            yield return null;
        }
        else
        {
            yield return new WaitForSeconds(time);
        }

        invokeDelegate();

        if (_dictionary.ContainsKey(id))
        {
            _dictionary.Remove(id);
        }
    }

    public void CancelApplyInvoke(uint id)
    {
        if (_dictionary.ContainsKey(id))
        {
            StopCoroutine(_dictionary[id]);
            _dictionary.Remove(id);
        }
    }

}
