﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BotBehavior : Mortal
{
    public enum TypeBot
    {
        Simple,
        Normal,
        
    }

    public event Action<CellItem> OnCollisionCellItem;
    
    [SerializeField] private LayerMask _layerMask;
    
    [SerializeField] private List<MovementComponent.DirectionMovement> _listDirection = new List<MovementComponent.DirectionMovement>();

    private Vector3 _currentDirection = Vector3.zero;

    private float _radiusCollider;
    public float RadiusCollider
    {
        get
        {
            if (_radiusCollider.Equals(0))
            {
                _radiusCollider = ((SphereCollider) _collider).radius;
            }

            return _radiusCollider;
        }
    }

    protected override void Awake()
    {
        base.Awake();
        
        _layerMask = LayerMask.GetMask("Cell");

        CellController.OnAddCellInTrack += AddCellInTrack;
        CellController.OnDestroyPlayerCell += DestroyPlayerCell;
    }

    protected override void Start()
    {
        
    }
    
    protected override void OnDestroy()
    {
        CellController.OnAddCellInTrack -= AddCellInTrack;
        CellController.OnDestroyPlayerCell -= DestroyPlayerCell;
    }
    
    public override void ApplySpawn(CellItem cellItem)
    {
        base.ApplySpawn(cellItem);

        SetupMovement();
    }

    private void StartMovement(Vector3 direction)
    {
        _currentDirection = direction;
        
        _movementComponent.StopMovement();
        StartMovement();
    }

    protected override bool StartMovement()
    {
        if (!base.StartMovement())
        {
            return false;
        }

        _currentCellItem = GetTargetMovement();
        
        if (_currentCellItem == null)
        {
            StopMovement();
            return false;
        }
        
        _movementComponent.StartMovementToTarget(_currentPoint, ChangeSettingMovement);

        return true;
    }

    private Vector3 _currentNormal;
    private Vector3 _currentPoint;
    private CellItem _currentCellItem;
        
    private CellItem GetTargetMovement()
    {
        RaycastHit hit;
        
        if (!Physics.Raycast(transform.position, _currentDirection, out hit, 1000, _layerMask))
        {
            Debug.Log("Не смог найти таргет!");
            return null;
        }

        _currentNormal = hit.normal;
        _currentPoint = hit.point + (transform.position - hit.point).normalized * RadiusCollider;
        
//        Debug.DrawLine(_currentPoint, _currentPoint + _currentNormal.normalized * 4 , Color.green, 5);
//        Debug.DrawLine(transform.position, transform.position + _currentDirection.normalized * 5 , Color.red, 5);
        
        return hit.collider.GetComponent<CellItem>();
    }

    private void ChangeSettingMovement()
    {
        _currentNormal = GetValidNormal(_currentNormal);
        
        StartMovement(Vector3.Reflect(_currentDirection, _currentNormal));
        Debug.DrawLine(transform.position, transform.position + _currentDirection.normalized * 5, Color.cyan, 3);
    }

    private Vector3 GetValidNormal(Vector3 normal)
    {
        RaycastHit hit;
        
        if (!Physics.Raycast(_currentCellItem.transform.position, _currentNormal, out hit, 1, _layerMask))
        {
            return normal;
        }

        float angle = Utils.SignedAngle(-_currentDirection, _currentNormal, Vector3.up);
//        Debug.Log("Нормаль в говне. Угол = " + angle);
        
        if (angle > 0)
        {
            normal = Quaternion.AngleAxis(-90, Vector3.up) * normal;
//            Debug.DrawLine(_currentPoint, _currentPoint + normal * 3 , Color.white, 5);
            return normal.normalized;
        }
        
        normal = Quaternion.AngleAxis(90, Vector3.up) * normal;
//        Debug.DrawLine(_currentPoint, _currentPoint + normal * 3 , Color.white, 5);
        return normal.normalized;
    }

    private void SetupMovement()
    {
        if (_listDirection.Count < 2)
        {
            Debug.Log("Нет ни одного доступного направления для перемещения.");
            return;
        }

        int randomValue = Random.Range(0, _listDirection.Count);
        MovementComponent.DirectionMovement directionMovement = _listDirection[randomValue];
        Vector3 direction = _movementComponent.GetDirection(directionMovement);


        StartMovement(direction);
    }

//    protected override void ApplyCollisionCell(CellItem cellItem, ContactPoint contactPoint)
//    {
//        base.ApplyCollisionCell(cellItem, contactPoint);
//
//        Vector3 dir = _lastPosition - cellItem.transform.position;
//        
//        
////        int a = (int)Utils.SignedAngle(-_lastDirection, contactPoint.normal, Vector3.up);
////        Debug.Log(Vector3.Angle(_lastDirection, contactPoint.normal));
////        Vector3 direction = Quaternion.AngleAxis(90, Vector3.up) * _lastDirection;
//        Vector3 direction = Vector3.Reflect(_lastDirection, contactPoint.normal);
//
//        StartMovement(direction);
//    }


    protected override Vector3 GetDesiredPositon(CellItem cellItem)
    {
        return cellItem.transform.position;
    }

    private void DestroyPlayerCell(CellItem cellItem)
    {
        if (_currentCellItem != cellItem)
        {
            return;
        }
    
        StartMovement(_currentDirection);
    }
    
    private void AddCellInTrack(CellItem cellItem)
    {
        RaycastHit hit;
        
        if (!Physics.Raycast(transform.position, _currentDirection, out hit, 1000, _layerMask))
        {
            Debug.Log("Не смог найти таргет !");
            return;
        }
        CellItem hitCellItem = hit.collider.GetComponent<CellItem>();

        if (hitCellItem != null && hitCellItem == cellItem)
        {
//            Debug.Log("change direction target point " + cellItem);
            StartMovement(_currentDirection);
            return;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        CellItem cellItem = other.gameObject.GetComponent<CellItem>();

        if (cellItem != null)
        {
            if (_currentCellItem.Equals(cellItem))
            {
                ChangeSettingMovement();
            }
            
            if (OnCollisionCellItem != null)
            {
                OnCollisionCellItem(cellItem);
            }
            
            return;
        }
        
        BotBehavior botBehavior = other.gameObject.GetComponent<BotBehavior>();
        
        if (botBehavior != null)
        {
            ContactPoint contactPoint = other.contacts[0];
            
            Vector3 direction = Vector3.Reflect(_currentDirection, contactPoint.normal);
            
            StartMovement(direction);
            
            return;
        }

        PlayerBehavior playerBehavior = other.gameObject.GetComponent<PlayerBehavior>();

        if (playerBehavior != null)
        {
            // TODO: write realization
            return;
        }
    }
}
